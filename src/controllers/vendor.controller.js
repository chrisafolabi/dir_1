const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const vendorService = require('../services/vendor.service');


const createVendor = catchAsync(async (req, res) => {
  const vendor = await vendorService.createVendor(req.body);
  res.status(httpStatus.CREATED).send(vendor);
});

const getVendors = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page','populate']);
  const result = await vendorService.queryVendors(filter, options);
  res.send(result);
});

const getVendor = catchAsync(async (req, res) => {
  let vendorId;

  if(req.url==='/me'){
      vendorId = req.user._id;
  }
  else{
    vendorId = req.params.vendorId;
  }

  let populate = [];
  const options = pick(req.query, ['populate']);

  if(options.populate){
    options.populate.split(',').forEach((populateOption) => {
      populate.push(populateOption);
    });
  }

  const vendor = await vendorService.getVendorById(vendorId,populate);

  if (!vendor) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Vendor not found');
  }
  res.send(vendor);
});

const updateVendor = catchAsync(async (req, res) => {
  let vendorId;
  if(req.url==='/me'){
    vendorId = req.user._id;
  }
  else{
    vendorId = req.params.vendorId;
  }
  const vendor = await vendorService.updateVendorById(vendorId, req.body);
  res.send(vendor);
});

const deleteVendor = catchAsync(async (req, res) => {
  await vendorService.deleteVendorById(req.params.vendorId);
  res.status(httpStatus.NO_CONTENT).send();
});

const searchVendors = catchAsync(async (req,res)=>{
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page','range']);
  const location = pick(req.query, ['lon','lat']);
  const result = await vendorService.searchVendors(filter, location, options);
  res.send(result);
})

module.exports = {
  createVendor,
  getVendors,
  getVendor,
  updateVendor,
  deleteVendor,
  searchVendors

};
