const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const categoriesService = require('../services/categories.service');

const createCategories = catchAsync(async (req, res) => {
  console.log(req.body);
  const categories = await categoriesService.createCategories(req.body);
  res.status(httpStatus.CREATED).send(categories);
});

const getCategories = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await categoriesService.queryCategories(filter, options);
  res.send(result);
});

const getCategory = catchAsync(async (req, res) => {
  const category = await categoriesService.getCategoriesById(req.params.categoryId);
  if (!category) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Category not found');
  }
  res.send(category);
});

const updateCategories = catchAsync(async (req, res) => {
  const categories = await categoriesService.updateCategoriesById(req.params.categoriesId, req.body);
  res.send(categories);
});

const deleteCategories = catchAsync(async (req, res) => {
  await categoriesService.deleteCategoriesById(req.params.categoriesId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createCategories,
  getCategories,
  getCategory,
  updateCategories,
  deleteCategories,
};
