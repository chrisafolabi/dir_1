const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const vendorService = require('../services/vendor.service');
const tokenService = require('../services/token.service');
const userService = require('../services/user.service');
const authService = require('../services/auth.service');
const emailService = require('../services/email.service');
const mailProcessor = require('../jobs/Mail/processor');
const adsProcessor = require('../jobs/Ads/processor');

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const registerVendor = catchAsync(async (req, res) => {
  console.log("creating");
  const vendor = await vendorService.createVendor(req.body);
  const tokens = await tokenService.generateAuthTokens(vendor);
  res.status(httpStatus.CREATED).send({ vendor, tokens });
});

const registerSocial = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const socialRegister = catchAsync(async (req, res) => {
  const user = await userService.createSocialUser(req.body);
  const tokens = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});

const socialLogin = catchAsync(async (req, res) => {
  const { email, type } = req.body;
  const user = await authService.loginSocialUser(email, type);
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const login = catchAsync(async (req, res) => {
  const { email, password, type } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password, type );
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(req.body.email);
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  userId = req.user._id;
  await authService.resetPassword(req.body.oldpassword, req.body.password , userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const changePassword = catchAsync(async (req, res) => {
  await authService.changePassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  registerVendor,
  socialLogin,
  socialRegister,
  changePassword
};
