const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await userService.queryUsers(filter, options);
  res.send(result);
});

const getUser = catchAsync(async (req, res) => {

  let userId;

  if(req.url==='/me'){
    userId = req.user._id;
  }
  else{
    userId = req.params.userId;
  }

  const user = await userService.getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const getUserBookmarks = catchAsync(async (req, res) => {

  let userId = req.user._id;


  const user = await userService.getUserBookmarksById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const getUserTimeline = catchAsync(async (req, res) => {

  let userId = req.user._id;
  const user = await userService.getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  const options = pick(req.query, ['sortBy', 'limit', 'page','populate']);
  let timeline = await userService.createTimeline(userId,options);

  res.send(timeline);
});

const updateUser = catchAsync(async (req, res) => {
  let userId;

  if(req.url==='/me'){
    userId = req.user._id;
  }
  else{
    userId = req.params.userId;
  }
  const user = await userService.updateUserById(userId, req.body);
  res.send(user);
});

const addDevice = catchAsync(async (req, res)=>{
  let device = {
    device_type : req.body.device_type,
    notification_id: req.body.notification_id,
  }

  const deviceNew = await userService.addDevice(req.body.userId, device);
  res.send(deviceNew);
})

const addBookmark = catchAsync(async (req, res) => {
  let userId;
  userId = req.user._id;

  const user = await userService.addBookmarkbyId(userId, req.body.vendor);
  res.send(user);

});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  addBookmark,
  getUserBookmarks,
  getUserTimeline,
  addDevice
};
