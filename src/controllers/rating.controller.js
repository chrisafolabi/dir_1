const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const ratingService = require('../services/rating.service');

const createRating = catchAsync(async (req, res) => {
  const rating = await ratingService.createRating(req.body);
  res.status(httpStatus.CREATED).send(rating);
});

const getRating = catchAsync(async (req, res) => {
  const rating = await ratingService.getRatingById(req.params.ratingId);
  if (!rating) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Rating not found');
  }
  res.send(rating);
});

const getRatings = catchAsync(async (req,res)=>{
  const filter = pick(req.query, ['name']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await ratingService.queryRatings(filter,options);
  res.send(result);
})

const updateRating = catchAsync(async (req, res) => {
  const rating = await ratingService.updateRatingById(req.params.ratingId, req.body);
  res.send(rating);
});

const deleteRating = catchAsync(async (req, res) => {
  await ratingService.deleteRatingById(req.params.ratingId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createRating,
  getRating,
  getRatings,
  updateRating,
  deleteRating,
};
