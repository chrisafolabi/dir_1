const AccountStatus = Object.freeze({
  PENDING: 0,
  ACTIVE: 1,
  BLOCKED: 2,
  INACTIVE: 3
});

const UserType = Object.freeze({
  USER: 0,
  VENDOR: 1,
  ADMIN: 2,
});


module.exports = {
  AccountStatus,
  UserType
}
