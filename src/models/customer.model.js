const mongoose = require('mongoose');
mongoosastic = require('mongoosastic');
const { toJSON, paginate, EZsearch } = require('./plugins');

const customerSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
    },
    profilepic: {
      type: String,
      required: true,
      trim: true,
    },
    bookmarks: {
      type: String,
      required: true,
      trim: true,
    },
  }
  // { collection: Customer }
);

customerSchema.plugin(toJSON);
customerSchema.plugin(paginate);
customerSchema.plugin(mongoosastic);

const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;
