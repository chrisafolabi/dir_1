const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const { AccountStatus } = require('../utils/enums');
const { pointSchema } = require('./helpers/schemas');
const { toJSON, paginate, EZsearch } = require('./plugins');
const random = require('mongoose-simple-random');
const _=require('underscore');

const { roles } = require('../config/roles');
const Schema = mongoose.Schema;
mongoosastic = require('mongoosastic');

const vendorSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    devices:[{ type: mongoose.Types.ObjectId, ref:'Device' }],
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      private: true
    },
    profilepic: {
      type: String,
    },
    headerpic: {
      type: String,

    },
    description: {
      type: String,
    },
    website: {
      type: String,
    },
    images: [{ type: String}],
    phone: { type: String, required: true },
    address: {
      type: String,
    },
    location: {
      type:pointSchema(),
      index: '2dsphere'
    },
    ads: [{type:mongoose.Types.ObjectId, ref:'Ads'}],
    ad_local: {type:Boolean, default: false},
    ad_international: {type:Boolean, default: false},
    rating: [{type:mongoose.Types.ObjectId, ref:'Rating'}],
    averageRating: {type:Number,default:0},
    status: {type:Number, enum: Object.values(AccountStatus), default: AccountStatus.PENDING},
    review: {
      type: String,
    },
    category: {required: true, type:mongoose.Types.ObjectId, ref:'Categories'},
    role: {
      type: String,
      enum: roles,
      default: 'vendor',
    },
  }
  // { collection: vendor }
);

// add plugin that converts mongoose to json
vendorSchema.plugin(toJSON);
vendorSchema.plugin(paginate);
vendorSchema.plugin(EZsearch);
vendorSchema.plugin(mongoosastic);
vendorSchema.plugin(random)


/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeVendorId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
vendorSchema.statics.isEmailTaken = async function (email, excludeVendorId) {
  const vendor = await this.findOne({ email, _id: { $ne: excludeVendorId } });
  return !!vendor;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
vendorSchema.methods.isPasswordMatch = async function (password) {
  const vendor = this;
  return bcrypt.compare(password, vendor.password);
};

vendorSchema.methods.isBookmarked = async function  (liked_list) {
   return _.contains(liked_list,this._id);
}
vendorSchema.pre('save', async function (next) {
  const vendor = this;
  if (vendor.isModified('password')) {
    vendor.password = await bcrypt.hash(vendor.password, 8);
  }
  next();
});



/**
 * @typedef Vendor
 */

const Vendor = mongoose.model('Vendor', vendorSchema);

module.exports = Vendor;
