const mongoose = require('mongoose');
const {Vendor} = require('../models/vendors.model');
const {User} = require('../models/user.model');

const { toJSON, paginate, EZsearch } = require('./plugins');

const adSchema = mongoose.Schema({
  vendor: { type: mongoose.Types.ObjectId, ref: 'Vendor'},
  expiryDate : {type: Number, required: true}
})

const Ads = mongoose.model("Ads",adSchema);
module.exports = Ads
