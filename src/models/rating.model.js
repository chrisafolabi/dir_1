const mongoose = require('mongoose');
const {Vendor} = require('../models/vendors.model');
const {User} = require('../models/user.model');

const { toJSON, paginate, EZsearch } = require('./plugins');
mongoosastic = require('mongoosastic');


const ratingSchema = mongoose.Schema(
  {
    number: {type: Number, required: true},
    review: { type: String },
    vendor: { type: mongoose.Types.ObjectId, ref: 'Vendor'},
    user: { type: mongoose.Types.ObjectId, ref: 'User'},

  }
  // { collection: Rating }
);

// add plugin that converts mongoose to json
ratingSchema.plugin(toJSON);
ratingSchema.plugin(paginate);
ratingSchema.plugin(EZsearch);
ratingSchema.plugin(mongoosastic);


const Rating = mongoose.model('Rating', ratingSchema);

module.exports = Rating;
