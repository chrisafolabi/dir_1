const mongoose = require('mongoose');
const { toJSON, paginate, EZsearch } = require('./plugins');

const deviceSchema = mongoose.Schema(
  {
    device_type: { type: String, required: true },
    notification_id: { type: String, private: true },
    last_used: { type: Date, default: Date.now, expires: 15552000000 },
  }
);

deviceSchema.plugin(toJSON);
deviceSchema.plugin(paginate);
deviceSchema.plugin(EZsearch);
const Device = mongoose.model('Device', deviceSchema);

module.exports = Device;
