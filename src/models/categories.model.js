const mongoose = require('mongoose');
const { toJSON, paginate, EZsearch } = require('./plugins');

const categoriesSchema = mongoose.Schema(
  {
    name: { type: String, index: true, unique: true, required: true },
    colour: { type: String, required: true },
    icon: { type: String, required: true },
  }
  //  { collection: Categories }
);

categoriesSchema.plugin(toJSON);
categoriesSchema.plugin(paginate);
categoriesSchema.plugin(EZsearch);
const Categories = mongoose.model('Categories', categoriesSchema);

module.exports = Categories;
