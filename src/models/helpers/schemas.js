const mongoose = require('mongoose');

const pointSchema = (required = false) => {
    return new mongoose.Schema({
        type: {
            type: String,
            enum: ['Point'],
            required
        },
        address: {
            type: String
        },
        coordinates: {
            type: [Number],
            required
        }
    });
}

module.exports = {
    pointSchema
}