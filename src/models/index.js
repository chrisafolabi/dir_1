module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Categories = require('./categories.model');
module.exports.Vendor = require('./vendors.model');
module.exports.Customer = require('./customer.model');
module.exports.Rating = require('./rating.model');
module.exports.Ads = require('./ads.model');
module.exports.Device = require('./device.model');

