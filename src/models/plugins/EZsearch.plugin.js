/* eslint-disable no-param-reassign */
const { utils } = require('../../config/utils');
const EZsearch = (schema) => {
  /**
   * @typedef {Object} QueryResult
   * @property {Document[]} results - Results found
   * @property {number} page - Current page
   * @property {number} limit - Maximum number of results per page
   * @property {number} totalPages - Total number of pages
   * @property {number} totalResults - Total number of documents
   */
  /**
   * Query for documents with pagination
   * @param {Object} [filter] - Mongo filter
   * @param location
   * @param {Object} [options] - Query options
   * @param {string} [options.sortBy] - Sorting criteria using the format: sortField:(desc|asc). Multiple sorting criteria should be separated by commas (,)
   * @param {number} [options.limit] - Maximum number of results per page (default = 10)
   * @param {number} [options.page] - Current page (default = 1)
   * @param {number} [options.range] - range for location query
   * @param {string} [options.populate] - fields to populate
   * @returns {Promise<QueryResult>}
   */
  schema.statics.EZsearch = async function (filter, location, options) {
    let sort = '';
    let populate = [];
    if (options.sortBy) {
      const sortingCriteria = [];
      options.sortBy.split(',').forEach((sortOption) => {
        const [key, order] = sortOption.split(':');
        sortingCriteria.push((order === 'desc' ? '-' : '') + key);
      });
      sort = sortingCriteria.join(' ');
    } else {
      sort = 'createdAt';
    }

    if (options.populate) {
      const populateCriteria = [];
      options.populate.split(',').forEach((populateOption) => {
        let p = populateOption;
        populateCriteria.push(populateOption);
      })
      populate = populateCriteria;
    };

    let range = utils.SEARCH_RANGE;
    const limit = options.limit && parseInt(options.limit, 10) > 0 ? parseInt(options.limit, 10) : 10;
    const page = options.page && parseInt(options.page, 10) > 0 ? parseInt(options.page, 10) : 1;
    if(options.range) {
      range = options.page && parseInt(options.range, 10) > 0 ? parseInt(options.range, 10) : 1;
    }
    const skip = (page - 1) * limit;

    let query = {};
    if(filter){
      query.name = filter.name
    }
    if(location.lon){
      query.location = {
        $near: {
          $maxDistance: range,
          $geometry: {
            type: "Point",
            coordinates: [location.lon, location.lat]
          }
        }
      }
    }


    let docsPromise =  this.find({
      location: query.location,
      name: (query.name) ? query.name:/.*/
    }).populate(populate).sort(sort).skip(skip).limit(limit).exec();

    let countPromise =  this.find({
      location: query.location,
      name: (query.name) ? query.name:/.*/
    }).exec();

    return Promise.all([countPromise, docsPromise]).then((values) => {
      const [totalResults, results] = values;
      const totalPages = Math.ceil(totalResults / limit);
      const result = {
        results,
        page,
        limit,
        totalPages,
        totalResults,
      };
      return Promise.resolve(result);
    });
  };
};

module.exports = EZsearch;
