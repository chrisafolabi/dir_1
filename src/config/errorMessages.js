const errorMessages = {

  //Auth
  INVALID_LOGIN: 'Invalid email/password provided',
  INVALID_SOCIAL: 'The provided social account is not connected to any user',
  SOCIAL_ALREADY_MEMBER: 'This email is linked to an account already',
  INVALID_SOCIAL_TOKEN: 'Provided token is expired or invalid'

};

module.exports = {
  errorMessages,
};
