const roles = ['user', 'admin', 'vendor'];

const roleRights = new Map();
roleRights.set(roles[0], ['manageRatings','getUsers', 'manageUsers','manageVendors', 'getVendors']);
roleRights.set(roles[1], ['getUsers', 'manageUsers','manageCategories','getCategories', 'manageRatings']);
roleRights.set(roles[2], ['manageVendors', 'getVendors']);


module.exports = {
  roles,
  roleRights,
};
