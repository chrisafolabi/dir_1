const fetch = require('node-fetch');
const ApiError = require('../utils/ApiError');
const { errorMessages } = require('../config/errorMessages');
const status = require('http-status');


const  getGoogleUserfromToken = () => async (req, res, next) => {
    let token = req.body.token;
    await fetch('https://www.googleapis.com/userinfo/v2/me', {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(res => res.json())
    .then((json) => {
      if(json.error){
        next(new ApiError(status.BAD_REQUEST, errorMessages.INVALID_SOCIAL_TOKEN));
      }
      req.body = json;
      next();
    })
    .catch((e)=>{
      next(e);
    });
}

const  getFacebookUserfromToken = () => async (req, res, next) => {
  let token = req.body.token;
  await fetch(`https://graph.facebook.com/me?fields=email,name&access_token=${token}`)
    .then(res => res.json())
    .then((json) => {
      if(json.error){
        next(new ApiError(status.BAD_REQUEST, errorMessages.INVALID_SOCIAL_TOKEN));
      }
      req.body = json;
      next();
    })
    .catch((e)=>{
      next(e);
    });
}


module.exports={
  getGoogleUserfromToken: getGoogleUserfromToken,
  getFacebookUserfromToken: getFacebookUserfromToken,
}
