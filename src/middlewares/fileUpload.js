const status = require('http-status');
const _=require('underscore');
var Busboy = require('busboy');
var AWS = require('aws-sdk');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');
const { v4: uuidv4 } = require('uuid');
const fs =  require('fs');

let S3 = new AWS.S3({
  apiVersion: '2006-03-01',
  region: config.s3.region,
  accessKeyId: config.aws.access,
  secretAccessKey: config.aws.secret
});


const uploadFile = (fileField="file") => (req,res,next)=>{
   const body = {};

    if (req.method ==="POST"){
      var busboy = new Busboy({ headers: req.headers });
      let chunks = [], fname, ftype, fEncoding, fileKey;


      busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

        if(fileField===fieldname) {

          console.log('File [' + fieldname + ']: filename: ' + filename + ', encoding: ' + encoding + ', mimetype: ' + mimetype);

          if (!filename) {
            // If filename is not truthy it means there's no file
            return;
          }
          fname = filename.replace(/ /g, "_");
          ftype = mimetype;
          fEncoding = encoding;
          file.fileRead = [];

          file.on('data', function(data) {
            this.fileRead.push(data);
            chunks.push(data);
          });

          file.on('error', function(err) {
            console.log('Error while buffering the stream: ', err);
          });

          file.on('end', function() {
          });
        }
        else{
          next(new ApiError(status.BAD_REQUEST, 'Missing field ' + fileField + ' is required'));
        }
      });


      busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        body[fieldname] = val;
      });

      busboy.on('finish', function() {
        if(!ftype){
          next(new ApiError(status.BAD_REQUEST, 'Missing field ' + fileField + ' is required'));
        }
        else {
          console.log('Done parsing form!');

          const params = {
            Bucket: "eventsza-dev", // your s3 bucket name
            Key: `${uuidv4()}.${ftype.split('/').pop()}`,
            Body: Buffer.concat(chunks), // concatinating all chunks
            ACL: 'public-read',
            ContentEncoding: fEncoding, // optional
            ContentType: ftype // required
          }
          // we are sending buffer data to s3.
          S3.upload(params, (err, s3res) => {
            if (err) {
              next(new ApiError(status.INTERNAL_SERVER_ERROR, err));
            } else {
              req.body = body;
              req.body[fileField] = s3res.Location;
              next();
            }
          });
        }


      });

      req.pipe(busboy);


    }
}

 const uploadMultipleFile = () => (req,res,next)=>{
  let fileKeys = Object.keys(req.files);

  let count = fileKeys.length;
  let i = 0;

  fileKeys.forEach(function(key) {

    let file = req.files[key][0];
    const params = {
      Bucket: "eventsza-dev", // your s3 bucket name
      Key: `${uuidv4()}.${file.mimetype.split('/').pop()}`,
      Body: fs.createReadStream(file.path), // concatinating all chunks
      ACL: 'public-read',
      ContentEncoding: file.encoding, // optional
      ContentType: file.mimetype // required
    }

    // we are sending buffer data to s3.
    S3.upload(params, (err, s3res) => {
      if (err) {
        next(new ApiError(status.INTERNAL_SERVER_ERROR, err));
      } else {
        console.log(s3res.Location);
        fs.unlinkSync(file.path);
        //req.body = body;
        req.body[file.fieldname] = s3res.Location;
        i++;
        if(i==count){
          next();
        }
      }
    });

      //files.push(req.files[key]);
  });

}


module.exports = {uploadFile, uploadMultipleFile};
