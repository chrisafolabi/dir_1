const status =require('http-status')
const ApiError = require('../utils/ApiError');
const { errorMessages } = require('../config/errorMessages');

const retrieveUserIdFromToken = ()=> async (req,res,next) => {
  req.body.userId = req.user._id;
  req.params.userId = req.user._id;
  next();
}
module.exports = {
  retrieveUserIdFromToken

}
