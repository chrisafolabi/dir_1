const { vendorService, userService } = require('../services');
const { UserType } = require('../utils/enums');
const {Vendor, User} = require('../models/index')
const _ = require('underscore');

const getUserbyId = async (userId, userType)=>{
  switch (userType){
    case UserType.VENDOR:
      return(vendorService.getVendorById(userId));
    default:
      return(userService.getUserById(userId));
  }
}

const getUserDevices = async(userId,userType)=>{
  switch (userType){
    case UserType.VENDOR:
      let vendor = await Vendor.findById(userId).populate('Device').exec()
      return(vendor.devices);
    default:
      let user = await User.findById(userId).populate('Device').exec()
      return(vendor.devices);
  }
}

module.exports ={
  getUserbyId,
  getUserDevices
}
