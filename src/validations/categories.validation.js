const Joi = require('@hapi/joi');
const { password, objectId } = require('./custom.validation');


const createCategories = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    colour: Joi.string().required(),
    icon: Joi.string().required(),
  }),
};

const getCategories = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getCategory = {
  params: Joi.object().keys({
    categoriesId: Joi.string().custom(objectId),
  }),
};

const updateCategories = {
  params: Joi.object().keys({
    categoriesId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
    name: Joi.string().required(),
    colour: Joi.string().required(),
    icon: Joi.string().required(),
    })
};

const deleteCategories = {
  params: Joi.object().keys({
    categoriesId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createCategories,
  getCategories,
  getCategory,
  updateCategories,
  deleteCategories,
};
