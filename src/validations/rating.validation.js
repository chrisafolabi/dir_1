const Joi = require('@hapi/joi');
const { password, objectId } = require('./custom.validation');

const createRating = {
  body: Joi.object().keys({
    number:  Joi.number().required().integer(),
    review: Joi.string(),
    vendorId: Joi.string().required().custom(objectId),
  }),
};

const getRatings = {
  query: Joi.object().keys({

  }),
};

const getRating= {
  params: Joi.object().keys({
    ratingId: Joi.string().custom(objectId),
  }),
};

const updateRating = {
  params: Joi.object().keys({
    ratingId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
        number:  Joi.number().integer(),
        review: Joi.string().required(),
    })
};

const deleteRating = {
  params: Joi.object().keys({
    ratingId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createRating,
  getRatings,
  getRating,
  updateRating,
  deleteRating,
};
