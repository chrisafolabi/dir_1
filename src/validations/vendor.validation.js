const Joi = require('@hapi/joi');
const { password, objectId } = require('./custom.validation');

const createVendor = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    category: Joi.string().required(),
    phone: Joi.string().required()
  }),
};

const getVendors = {
  query: Joi.object().keys({
    name: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getVendor= {
  params: Joi.object().keys({
    vendorId: Joi.string().custom(objectId),
  }),
};


const searchVendor= {
  query: Joi.object().keys({
    name: Joi.string(),
    lon: Joi.number().when("name", {
      is: null,
      then: Joi.required()
    }),
    lat: Joi.number().when("lon", {
      is: Joi.exist(),
      then: Joi.required()
    }),
    sortBy: Joi.string(),
    range: Joi.number().integer(),
    category: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  })
};

const updateVendor = {
  params: Joi.object().keys({
    vendorId: Joi.custom(objectId),
  }),
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      password: Joi.string().custom(password),
      name: Joi.string(),
      address: Joi.string(),
      coordinates: Joi.array()
    })
    .min(1),
};

const deleteVendor = {
  params: Joi.object().keys({
    vendorId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createVendor,
  getVendors,
  getVendor,
  updateVendor,
  deleteVendor,
  searchVendor
};
