const Joi = require('@hapi/joi');
const { password, objectId } = require('./custom.validation');

const createAd = {
  body: Joi.object().keys({
    vendorId:  Joi.string().custom(objectId),
    expiryDate: Joi.string(),
    type: Joi.string().required().valid('local', 'national'),
  }),
};

const getAds = {
  query: Joi.object().keys({

  }),
};

const getAd= {
  params: Joi.object().keys({
    ratingId: Joi.string().custom(objectId),
  }),
};

const updateAd = {
  params: Joi.object().keys({
    adId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      number:  Joi.number().integer(),
      review: Joi.string().required(),
    })
};

const deleteAd = {
  params: Joi.object().keys({
    ratingId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createAd,
  getAd,
  getAds,
  updateAd,
  deleteAd,
};
