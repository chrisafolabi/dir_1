const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const ratingValidation = require('../../validations/rating.validation');
const ratingController = require('../../controllers/rating.controller');

const router = express.Router();

router
  .route('/')
  .post(auth('manageRatings'), validate(ratingValidation.createRating), ratingController.createRating)
  .get(validate(ratingValidation.getRatings), ratingController.getRatings);

router
  .route('/:ratingId')


module.exports = router;

