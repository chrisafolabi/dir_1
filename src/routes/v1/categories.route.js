const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const categoriesValidation = require('../../validations/categories.validation');
const categoriesController = require('../../controllers/categories.controller');
const uploadFile = require('../../middlewares/fileUpload');

const router = express.Router();

router
  .route('/')
  .post(uploadFile.uploadFile('icon'),auth('manageCategories'), validate(categoriesValidation.createCategories), categoriesController.createCategories)
  .get(auth('getCategories'), validate(categoriesValidation.getCategories), categoriesController.getCategories);

router
  .route('/:categoriesId')

module.exports = router;

