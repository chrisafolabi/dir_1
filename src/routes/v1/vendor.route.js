const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const vendorValidation = require('../../validations/vendor.validation');
const vendorController = require('../../controllers/vendor.controller');
const uploadFile = require('../../middlewares/fileUpload');

const router = express.Router();

router
  .route('/')
  .post(auth('manageVendors'),  validate(vendorValidation.createVendor), vendorController.createVendor)
  .get(auth('getVendors'), validate(vendorValidation.getVendors), vendorController.getVendors);

router
  .route('/me')
  .get(auth('getVendors'),  vendorController.getVendor)
  .patch(auth('getVendors'), validate(vendorValidation.updateVendor), vendorController.updateVendor)

router
  .route('/search')
  .get(auth('getVendors'), validate(vendorValidation.searchVendor), vendorController.searchVendors)


router
  .route('/:vendorId')
  .get(auth('getVendors'), validate(vendorValidation.getVendor), vendorController.getVendor)
  .patch(auth('getVendors'), validate(vendorValidation.updateVendor), vendorController.updateVendor)




module.exports = router;

