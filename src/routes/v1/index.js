const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const vendorRoute = require('./vendor.route');
const ratingRoute = require('./rating.route');
const categoriesRoute = require('./categories.route');


const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/docs', docsRoute);
router.use('/vendors', vendorRoute);
router.use('/ratings', ratingRoute);
router.use('/categories', categoriesRoute);

module.exports = router;
