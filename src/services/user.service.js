const httpStatus = require('http-status');
const { User,Device } = require('../models');
const ApiError = require('../utils/ApiError');
const vendorService = require('./vendor.service');
const _=require('underscore');
const { errorMessages } = require('../config/errorMessages');
const generator = require('generate-password');


/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  const user = await User.create(userBody);
  return user;
};

/**
 * Create a user with social sign up
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createSocialUser = async (userBody) => {

  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, errorMessages.SOCIAL_ALREADY_MEMBER);
  }

  let info = {
    email: userBody.email,
    name: userBody.name,
    password: generator.generate({
      length: 10,
      numbers: true,
      symbols:true
    })
  }

  const user = await User.create(info);
  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserBookmarksById = async (id) => {
  return User.findById(id).populate({
    path: 'bookmarks',
    populate: {
      path: 'category',
      model: 'Categories'
    }
 });
};


/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};


/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} vendorId
 * @returns {Promise<User>}
 */

const addBookmarkbyId = async (userId, vendorId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  const vendor = await vendorService.getVendorById(vendorId);
  if (!vendor) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Vendor Id not valid');
  }

  user.bookmarks.push(vendor._id);

  user.save();

  return user.populate('bookmarks');

}


/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} device
 * @returns {Promise<User>}
 */

const addDevice = async (userId, device) => {
  let user = await User.findById(userId).populate('devices');

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }

  let check_device =_.findWhere(user.devices,{notification_id:device.notification_id});
  if(check_device!==undefined){
    return check_device;
  }

  const newDevice = await Device.create(device);
  user.devices.push(newDevice._id);
  user.save();
  return newDevice;
}

/**
 * create user timeline
 * @param {ObjectId} userId
 * @returns {Array} timeline
 */

const createTimeline = async (userId,options)=>{
    //todo: add location filter to top
    let topVendors = await vendorService.getTopVendors(5);
    let vendors = await vendorService.searchVendors({},{},options);
    const timeline = [].concat(topVendors.local,topVendors.national, vendors.results);

    return { timeline: timeline, pagination: _.omit(vendors,'results','totalResults')};

}

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};



module.exports = {
  createUser,
  createTimeline,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  addBookmarkbyId,
  createSocialUser,
  getUserBookmarksById,
  addDevice
};
