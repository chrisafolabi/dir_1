const httpStatus = require('http-status');
const { Vendor } = require('../models');
const ApiError = require('../utils/ApiError');
const _=require('underscore');
const categoriesService = require('../services/categories.service');

/**
 * Create a Vendor
 * @param {Object} vendorBody
 * @returns {Promise<Vendor>}
 */
const createVendor = async (vendorBody) => {
  if (await Vendor.isEmailTaken(vendorBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }



  if(await categoriesService.getCategoriesById(vendorBody.category)){
    const vendor = await Vendor.create(vendorBody);
    return vendor;
  }
  else{
    throw new ApiError(httpStatus.BAD_REQUEST, 'Category Invalid');
  }

};

/**
 * Query for vendors
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryVendors = async (filter, options) => {
  const vendors = await Vendor.paginate(filter, options);
  return vendors;
};

/**
 * Create a Vendor
 * @param {Number} count
 * @returns {Object} top vendors
 */

const getTopVendors = async (count=5)=>{
    let localTop = await Vendor.find({ ad_local: true },{}, {limit: count}).exec();
    let nationalTop = await Vendor.find({ ad_international: true },{}, {limit: count}).exec();
    return{
      local:_.shuffle(localTop),
      national:_.shuffle(nationalTop)
    }
}

/**
 * Search for vendors
 * @param {Object} filter - Mongo filter
 * @param {Object} location - location of user
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @param {number} [options.populate] - populate result

 * @returns {Promise<QueryResult>}
 */
const searchVendors = async (filter, location, options) => {
  if(filter['name']) {
    filter['name'] = { $regex: filter['name']};
  }
  const vendors = await Vendor.EZsearch(filter, location, options);
  return vendors;
};

/**
 * Get vendor by id
 * @param {ObjectId} id
 * @returns {Promise<Vendor>}
 */
const getVendorById = async (id,populate) => {
  return (await Vendor.findById(id).populate(populate).exec());
};

/**
 * Get vendor by email
 * @param {string} email
 * @returns {Promise<Vendor>}
 */
const getVendorByEmail = async (email) => {
  return Vendor.findOne({ email });
};





/**
 * Update vendor by id
 * @param {ObjectId} vendorId
 * @param {Object} updateBody
 * @returns {Promise<Vendor>}
 */
const updateVendorById = async (vendorId, updateBody) => {
  const vendor = await Vendor.findById(vendorId);
  if (!vendor) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Vendor not found');
  }
  if (updateBody.email && (await Vendor.isEmailTaken(updateBody.email, vendorId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  if(updateBody.coordinates && updateBody.address){
    updateBody.location = {
        "type" : "Point",
        "address": updateBody.address,
        "coordinates": updateBody.coordinates
    }
  }
  Object.assign(vendor, updateBody);
  await vendor.save();
  return vendor;
};

/**
 * Delete vendor by id
 * @param {ObjectId} vendorId
 * @returns {Promise<Vendor>}
 */
const deleteVendorById = async (vendorId) => {
  const vendor = await getVendorById(vendorId);
  if (!vendor) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Vendor not found');
  }
  await vendor.remove();
  return vendor;
};

const addRating = async (vendorId,rating) => {
  const vendor = await Vendor.findById(vendorId).populate('rating');
  if (!vendor) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Vendor not found');
  }

  let avg;

  if (vendor.rating.length === 0) {

    avg = rating.number;
  } else {

  var sum = _.reduce(vendor.rating, function(memo, num){
    console.log("in js");
    console.log(memo);
    console.log(num);
    return memo + num.number;
  }, 0);

  // var sum = vendor.rating.reduce(function(a,b){

  //   return a+b.number
  // }, 0);
  avg = (sum+rating.number)/(vendor.rating.length+1);


  }

  vendor.averageRating = avg;
  vendor.rating.push(rating._id);

  await vendor.save();

  return vendor;
};


module.exports = {
  createVendor,
  queryVendors,
  getVendorById,
  getVendorByEmail,
  updateVendorById,
  deleteVendorById,
  addRating,
  getTopVendors,
  searchVendors
};
