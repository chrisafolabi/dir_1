const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { Categories } = require('../models');

/**
 * Create a Categories
 * @param {Object} categoriesBody
 * @returns {Promise<Categories>}
 */
const createCategories = async (categoriesBody) => {
  console.log(categoriesBody);
  const categories = await Categories.create(categoriesBody);
  return categories;
};

/**
 * Query for categoriess
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryCategories = async (filter, options) => {
  const categories = await Categories.paginate(filter, options);
  return categories;
};

/**
 * Get categories by id
 * @param {ObjectId} id
 * @returns {Promise<Categories>}
 */
const getCategoriesById = async (id) => {

  let category;
  try{
    category = Categories.findById(id);
  }
  catch(err){
    throw new ApiError(httpStatus.BAD_GATEWAY, 'Invalid Id');
  }
  return category;
};

/**
 * Update categories by id
 * @param {ObjectId} categoriesId
 * @param {Object} updateBody
 * @returns {Promise<Categories>}
 */
const updateCategoriesById = async (categoriesId, updateBody) => {
  const categories = await getCategoriesById(categoriesId);
  if (!categories) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Categories not found');
  }
  Object.assign(categories, updateBody);
  await categories.save();
  return categories;
};

/**
 * Delete categories by id
 * @param {ObjectId} categoriesId
 * @returns {Promise<Categories>}
 */
const deleteCategoriesById = async (categoriesId) => {
  const categories = await getCategoriesById(categoriesId);
  if (!categories) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Categories not found');
  }
  await categories.remove();
  return categories;
};

module.exports = {
  createCategories,
  queryCategories,
  getCategoriesById,
  updateCategoriesById,
  deleteCategoriesById,
};
