const userHelper = require('../helper/user.helper');
const ApiError = require('../utils/ApiError');
const { Expo } = require('expo-server-sdk');
const httpStatus = require('http-status');

let expo = new Expo();

const validToken = (pushToken)=>{
  if (!Expo.isExpoPushToken(pushToken)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Push token Invalid');
  }
  else {
    return true;
  }
}

const sendMessages = async (messages)=>{
  let chunks = expo.chunkPushNotifications(messages);
  let tickets = [];
  for (let chunk of chunks) {
    try {
      let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
      console.log(ticketChunk);
      tickets.push(...ticketChunk);
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }
}


const sendNotifactiontoId = async (userId,userType,message)=>{
    let user = await userHelper.getUserbyId(userId,userType);
    let messages = [];
    let devices = [];
    let pushToken = '';
    if(user==null){
      throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
    }
    devices = await userHelper.getUserDevices(userId,userType);
    for(let device of devices){
      pushToken = user.device;
      if(validToken(pushToken)){
        messages.push({
          to: pushToken,
          sound: 'default',
          header: message.header,
          body: message.body,
          data: message.data
        })
    }

      await sendMessages(messages);
      return {status:"success"}
    }
}

const sendNotifactiontoList = async (userIds,userType,message)=>{
  for(let user of userIds){
    await sendNotifactiontoId(user._id,userType,message);
  }
}

const sendNotifactiontoRegion = async (location,userType)=>{

}

const sendNotifactiontoAll = async (userType)=>{

}

module.exports = {
  sendNotifactiontoAll,
  sendNotifactiontoId,
  sendNotifactiontoRegion,
  sendNotifactiontoRegion,
  sendNotifactiontoList
}



