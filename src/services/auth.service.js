const httpStatus = require('http-status');
const tokenService = require('./token.service');
const userService = require('./user.service');
const vendorService = require('./vendor.service');
const Token = require('../models/token.model');
const ApiError = require('../utils/ApiError');
const { errorMessages } = require('../config/errorMessages');
const { tokenTypes } = require('../config/tokens');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password, type) => {
  if (type === "vendor") {

    const vendor= await vendorService.getVendorByEmail(email);
  if (!vendor || !(await vendor.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, errorMessages.INVALID_LOGIN);
    }
    return vendor;

  }
  const user = await userService.getUserByEmail(email);
  if (!user || !(await user.isPasswordMatch(password))) {
    let a = new ApiError(httpStatus.UNAUTHORIZED, errorMessages.INVALID_LOGIN);

    throw new ApiError(httpStatus.UNAUTHORIZED, errorMessages.INVALID_LOGIN);
  }
  return user;
};

/**
 * Login with social email
 * @param {string} email
 * @param {string} type
 * @returns {Promise<User>}
 */
const loginSocialUser = async (email,type) => {
  if (type === "vendor") {
    const vendor= await vendorService.getVendorByEmail(email);
    return vendor;
  }
  const user = await userService.getUserByEmail(email);
  if (!user) {
    throw new ApiError(httpStatus.UNAUTHORIZED, errorMessages.INVALID_SOCIAL);
  }
  return user;
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise}
 */
const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({ token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await refreshTokenDoc.remove();
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<Object>}
 */
const refreshAuth = async (refreshToken) => {
  try {
    const refreshTokenDoc = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
    const user = await userService.getUserById(refreshTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await refreshTokenDoc.remove();
    return tokenService.generateAuthTokens(user);
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(resetPasswordToken, tokenTypes.RESET_PASSWORD);
    const user = await userService.getUserById(resetPasswordTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await Token.deleteMany({ user: user.id, type: tokenTypes.RESET_PASSWORD });
    await userService.updateUserById(user.id, { password: newPassword });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password reset failed');
  }
};

/**
 * Change password
 * @param {string} oldPassword
 * @param {string} newPassword
 * @param {string} id
 * @returns {Promise}
 */
const changePassword = async (oldPassword, newPassword, id) => {
  try {
    const user = await userService.getUserById(id);
    if (!user || !(await user.isPasswordMatch(oldPassword))) {
      throw new ApiError(httpStatus.UNAUTHORIZED, errorMessages.INVALID_LOGIN);
    }
    await userService.updateUserById(user.id, { password: newPassword });

  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password change failed');
  }
};

module.exports = {
  loginUserWithEmailAndPassword,
  changePassword,
  logout,
  refreshAuth,
  resetPassword,
  loginSocialUser
};
