
const mailQueue = require('./index');
const helper = require('../helper');

// Wait 5 seconds before process and try to run 3 times
const jobOptions = {
  delay: 5000,
  attempts: 3,
};

const addToQueue = function(data,date=null){
  let options = jobOptions;
  if(date!=null){
    options.delay = helper.runAt(date);
  }
  mailQueue.add(data,options);
}


module.exports={
  addToQueue
}
