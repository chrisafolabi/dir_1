const express = require('express');

const Queue = require('bull');
const { sendMail } = require('./mail.jobs');


//init
const mailQueue = new Queue('mail', {
  redis: {
    host: '127.0.0.1',
    port: 6379,
    password: 'root',
    db: 1
  }
});

//processes
mailQueue.process(sendMail);

// events
mailQueue.on('completed', (job, result) => {
  console.log(`Job completed with result ${result}`);
})

module.exports = mailQueue;
