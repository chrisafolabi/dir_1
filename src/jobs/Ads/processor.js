
const adsQueue = require('./index');
const helper = require('../helper');

// Wait 5 seconds before process and try to run 3 times
const jobOptions = {
  delay: 1000,
  attempts: 3,
};

const addToLocalQueue = function(data){
  let options = jobOptions;
  options.delay = helper.runAt(data.expiryDate);
  adsQueue.add('local',data,options);
}

const addToNationalQueue = function(data){
  let options = jobOptions;
  options.delay = helper.runAt(data.expiryDate);
  adsQueue.add('national',data,options);
}


module.exports={
  addToLocalQueue,
  addToNationalQueue
}
