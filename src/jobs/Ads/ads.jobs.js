const vendorService = require('../../services/vendor.service');

const deactivateNationalTop = async function(job) {
  let updateBody = {ad_international:true}
  let vendor = vendorService.updateVendorById(job.data.id,updateBody);
  return vendor;
}

const deactivateLocalTop = async function(job) {
  let updateBody = {ad_local:true}
  let vendor = vendorService.updateVendorById(job.data.id,updateBody);
  return vendor;
}

module.exports={
  deactivateLocalTop,
  deactivateNationalTop
}
