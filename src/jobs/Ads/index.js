const Queue = require('bull');
const { deactivateLocalTop, deactivateNationalTop } = require('./ads.jobs');
const { sendMail } = require('./ads.jobs');


const adsQueue = new Queue('ads', {
  redis: {
    host: '127.0.0.1',
    port: 6379,
    password: 'root',
    db: 0
  }
});

//processes
adsQueue.process('local',deactivateLocalTop);
adsQueue.process('national',deactivateNationalTop);

// events
adsQueue.on('completed', (job, result) => {
  console.log(`Job completed with result ${result}`);
})

module.exports = adsQueue;
