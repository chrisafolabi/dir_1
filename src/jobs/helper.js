const moment = require('moment');

/**
 * Calculate amount of delay to run job at certain time
 * @param {Number} timestamp
 * @returns {Number}
 */
const runAt = function(timestamp){

  let CurrentDate = moment().format('X');
  let delay = timestamp - parseInt(CurrentDate);

  if(isNaN(delay) || delay < 0){
    return 0;
  }
  else {
    return (delay * 1000);
  }

}

module.exports={
  runAt
}
